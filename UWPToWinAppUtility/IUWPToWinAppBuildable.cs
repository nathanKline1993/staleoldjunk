﻿namespace Com.Me.Nwk25.UWPToWinAppUtility
{
    /// <summary>
    /// An interface that all Decorator objects should implement.
    /// </summary>
    public interface IUWPToWinAppBuildable
    {
        /// <summary>
        /// The specific code that decorator object will run.
        /// </summary>
        /// <returns>Returns true if operation was successfully, false otherwise.</returns>
        bool PerformComponentOperation();
    }
}
