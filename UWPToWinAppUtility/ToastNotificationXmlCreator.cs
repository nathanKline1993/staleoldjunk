﻿using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace Com.Me.Nwk25.UWPToWinAppUtility
{
    /// <summary>
    /// Houses static methods for creating XmlDocuments for toast notifications.
    /// </summary>
    public static class ToastNotificationXmlCreator
    {
        /// <summary>
        /// Creates XML code for simple toast notification with one text field.
        /// </summary>
        /// <param name="text">Text to displayed in the toast notification.</param>
        /// <returns>An XmlDocument that wraps the XML for the toast notification.</returns>
        public static XmlDocument CreateSimpleToastNotificationText(string text)
        {
            XmlDocument simpleToastNotificationXML = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText01);

            XmlNodeList xmlTextNode = simpleToastNotificationXML.GetElementsByTagName("text");
            xmlTextNode[0].AppendChild(simpleToastNotificationXML.CreateTextNode(text));

            return simpleToastNotificationXML;
        }
    }
}
