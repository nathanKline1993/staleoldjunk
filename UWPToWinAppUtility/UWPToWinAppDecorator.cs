﻿namespace Com.Me.Nwk25.UWPToWinAppUtility
{
    /// <summary>
    /// Abstract class for all Universal Windows Project to Windows App Project decorator objects. Mainly for the support of toast 
    /// notifications, which are not easily supported for Windows App projects.
    /// </summary>
    public abstract class UWPToWinAppDecorator
    {
        protected IUWPToWinAppBuildable component;

        /// <summary>
        /// AppUserModelID for the shortcut needed for the toast notification. Gives your toast notification a sense of direction.
        /// </summary>
        public string AppUserModelID { get; set; }

        /// <summary>
        /// Links the decorator components together.
        /// </summary>
        /// <param name="component">Sets the component to invoke in CallTrailer. Allows for components to link.</param>
        /// <param name="appUserModelID">Initializes the AppUserModelID property.</param>
        public UWPToWinAppDecorator(IUWPToWinAppBuildable component, string appUserModelID)
        {
            this.component = component;
            AppUserModelID = appUserModelID;
        }

        /// <summary>
        /// Calls the decorator's component PerformComponentOperation method.
        /// </summary>
        /// <returns>Returns true if the component is not null and successfully performs its operation, false otherwise.</returns>
        public bool CallTrailer()
        {
            if (component != null)
            {
                return component.PerformComponentOperation();
            }

            return false;
        }
    }
}
