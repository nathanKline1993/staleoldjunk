﻿using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace Com.Me.Nwk25.UWPToWinAppUtility
{
    /// <summary>
    /// Class to create shortcut for toast notification of an application.
    /// </summary>
    public class CreateShortcutToastNotificationDecorator: UWPToWinAppDecorator, IUWPToWinAppBuildable
    {
        /// <summary>
        /// The FileInfo of the executable file that the shortcut will be created for.
        /// </summary>
        public FileInfo ExeFile { get; set; }

        /// <summary>
        /// The desired shortcut file path.
        /// </summary>
        public string ShortcutFilePath { get; set; }

        /// <summary>
        /// Constructor for CreateShortcutToastNotificationDecorator.
        /// </summary>
        /// <param name="component">Passed up to the base class UWPToWinAppDecorator.</param>
        /// <param name="appUserModelID">Passed up to the base class UWPToWinAppDecorator.</param>
        /// <param name="exeFile">Used to the initialize the ExeFile property.</param>
        /// <param name="shortcutFilePath">Used to the initialize the ShortcutFilePath property.</param>
        public CreateShortcutToastNotificationDecorator(IUWPToWinAppBuildable component, string appUserModelID, 
            FileInfo exeFile, string shortcutFilePath): base(component, appUserModelID)
        {
            ExeFile = exeFile;
            ShortcutFilePath = shortcutFilePath;
        }

        /// <summary>
        /// Runs the ShortcutEditor executable.
        /// </summary>
        /// <returns>Indicates whether or not the shortcut was created.</returns>
        public bool PerformComponentOperation()
        {
            Process shortcutEditorProcess = new Process();
            Regex successDoneKeywordMatch = new Regex(Properties.Resources.SuccessfulStdOutShortcutEditorRegex);

            shortcutEditorProcess.StartInfo.UseShellExecute = false;
            shortcutEditorProcess.StartInfo.RedirectStandardOutput = true;
            shortcutEditorProcess.StartInfo.FileName = Properties.Resources.ShortcutEditorExePath;
            shortcutEditorProcess.StartInfo.Arguments = '"' + ExeFile.FullName + "\" \"" + ShortcutFilePath + "\" \"" + AppUserModelID + '"';

            shortcutEditorProcess.Start();
            string output = shortcutEditorProcess.StandardOutput.ReadToEnd();
            shortcutEditorProcess.WaitForExit();

            if (successDoneKeywordMatch.IsMatch(output))
            {
                return base.CallTrailer();
            }

            return false;
        }
    }
}
