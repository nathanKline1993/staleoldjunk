﻿using System.IO;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace Com.Me.Nwk25.UWPToWinAppUtility
{
    /// <summary>
    /// Decorator class for displaying toast notification after an application completes.
    /// </summary>
    public class ToastNotificationDecorator : UWPToWinAppDecorator, IUWPToWinAppBuildable
    {
        private FileInfo toastNotificationXmlFileInfo = null;
        private XmlDocument toastNotificationXml = null;

        /// <summary>
        /// Constructor for ToastNotificationDecorator.
        /// </summary>
        /// <param name="component">Passed up to the base class UWPToWinAppDecorator.</param>
        /// <param name="appUserModelID">Passed up to the base class UWPToWinAppDecorator.</param>
        /// <param name="toastNotificationXmlFileInfo">File containing the XML that will be used for the toast notification.</param>
        public ToastNotificationDecorator(IUWPToWinAppBuildable component, string appUserModelID, FileInfo toastNotificationXmlFileInfo) :
            base(component, appUserModelID)
        {
            this.toastNotificationXmlFileInfo = toastNotificationXmlFileInfo;
        }

        /// <summary>
        /// Constructor for ToastNotificationDecorator.
        /// </summary>
        /// <param name="component">Passed up to the base class UWPToWinAppDecorator.</param>
        /// <param name="appUserModelID">Passed up to the base class UWPToWinAppDecorator.</param>
        /// <param name="toastNotificationXml">XML text wrapped in a XmlDocument used for the toast notification.</param>
        public ToastNotificationDecorator(IUWPToWinAppBuildable component, string appUserModelID, XmlDocument toastNotificationXml) : 
            base(component, appUserModelID)
        {
            this.toastNotificationXml = toastNotificationXml;
        }

        /// <summary>
        /// Determines if the application ran successfully. If so, loads XML from file or text and displays the toast notification.
        /// </summary>
        /// <returns>True if the application succeed and the toast notification displayed, false otherwise.</returns>
        public bool PerformComponentOperation()
        {
            bool trailerResult = base.CallTrailer();
            
            if (!trailerResult)
            {
                return trailerResult;
            }

            XmlDocument toastNotificationXmlFromFileOrText;

            if (toastNotificationXmlFileInfo != null)
            {
                toastNotificationXmlFromFileOrText = new XmlDocument();
                toastNotificationXmlFromFileOrText.LoadXml(File.ReadAllText(toastNotificationXmlFileInfo.FullName));
            } else
            {
                toastNotificationXmlFromFileOrText = toastNotificationXml;
            }

            ToastNotification toastNotification = new ToastNotification(toastNotificationXmlFromFileOrText);
            ToastNotificationManager.CreateToastNotifier(AppUserModelID).Show(toastNotification);

            return true;
        }
    }
}
