// GeneralUtilities.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "GeneralUtilities.h"

/*
* Namespace for general functions used by other C++ projects.
*/
namespace GeneralUtilities {
	/*
	* Name: ConvertCharPntToWchar_TPnt
	* Summary: Static function of the static class, StringUtilities. Converts a 
	* normal char (ASCII) string to a wchar_t (2-bytes) string.
	* Parameters:
	*	string - Must be a normal char pointer.
	* Return: Same string as the "string" variable but the characters are now 2-bytes instead of 1.
	*/
	wchar_t *StringUtilities::ConvertCharPntToWchar_TPnt(char *string) {
		size_t numOfCharConverted;

		const size_t strLength = strlen(string) + 1;
		wchar_t *newWchar_tStr = new wchar_t[strLength];
		mbstowcs_s(&numOfCharConverted, newWchar_tStr, strLength, string, strLength);

		return newWchar_tStr;
	}
}
