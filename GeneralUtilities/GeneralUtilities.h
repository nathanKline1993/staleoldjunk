#pragma once

#ifdef GENERALUTILITIES_EXPORTS
#define GENERALUTILITIES_API __declspec(dllexport)
#else
#define GENERALUTILITIES_API __declspec(dllimport) 
#endif // GENERALUTILITIES_EXPORTS

using namespace std;

namespace GeneralUtilities {
	class StringUtilities
	{
	public:
		static GENERALUTILITIES_API wchar_t *ConvertCharPntToWchar_TPnt(char *string);
	};
}
