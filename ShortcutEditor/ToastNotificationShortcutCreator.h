#pragma once
using namespace Microsoft::WRL;
using namespace std;

class ToastNotificationShortcutCreator
{
private:
	wchar_t *exePath;
	wchar_t *appUserModelID;

public:
	ToastNotificationShortcutCreator(wchar_t *exePath, wchar_t *appUserModelID);
	~ToastNotificationShortcutCreator();
	
	HRESULT CreateShortcut(wchar_t *shortcutPath);
};
