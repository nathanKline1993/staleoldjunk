// ShortcutEditor.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ToastNotificationShortcutCreator.h"

using namespace std;

/*
* Name: main
* Summary: Main entry point.
* Parameters:
*	argc - there must be 4 arguments include the name of the command 
*	ShortcutEditor.exe.
*	argv - must include name of the command, ShortcutEditor.exe, the path of 
*	the executable, the path of the shortcut you want to make, and 
*	AppUserModelID you want to use for toast notifications.
* Return: 0 if successful or exits prematurely if command was invoke 
* incorrectly.
*/
int main(int argc, char *argv[])
{
	if (argc != 4) {
		cerr << "error: format of command not supported. Currently only used to create shortcuts for toast notifications." << endl;
		cerr << endl << "ShortcutEditor.exe <exeFilePath> <lnkFilePath> <appUserModelID>" << endl;
		exit(EXIT_FAILURE);
	}

	wchar_t *exeFilePath = StringUtilities::ConvertCharPntToWchar_TPnt(argv[1]);
	wchar_t *lnkFilePath = StringUtilities::ConvertCharPntToWchar_TPnt(argv[2]);
	wchar_t *appUserModelId = StringUtilities::ConvertCharPntToWchar_TPnt(argv[3]);

	ToastNotificationShortcutCreator shortcutCreator(exeFilePath, appUserModelId);
	HRESULT result = shortcutCreator.CreateShortcut(lnkFilePath);

	if (SUCCEEDED(result)) {
		cout << "done" << endl;
	}
	else {
		cout << "error: unable to create shortcut" << endl;
	}

    return 0;
}

