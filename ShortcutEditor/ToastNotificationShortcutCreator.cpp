#include "stdafx.h"
#include "ToastNotificationShortcutCreator.h"

wchar_t *exePath;
wchar_t *appUserModelID;

/*
* Name: ToastNotificationShortcutCreator
* Summary: Constructor of the ToastNotificationShortcutCreator class. Runs the 
* CoInitializeEx() function to set up the context for creating the shortcut.
* Initializes the path of the executable, and AppUserModelID.
* Parameters:
*	exePath - Location of the executable that needs a shortcut.
*	appUserModelID - The ID you will use for identification for toast 
*	notifications.
*/
ToastNotificationShortcutCreator::ToastNotificationShortcutCreator(wchar_t *exePath, wchar_t *appUserModelID)
{
	HRESULT hr;

	hr = CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED);

	if (!SUCCEEDED(hr)) {
		throw exception("Unable to initialize the COM library; CoInitializeEx failed\n");
	}

	this->exePath = exePath;
	this->appUserModelID = appUserModelID;
}


/*
* Name: ToastNotificationShortcutCreator
* Summary: Destructor of the ToastNotificationShortcutCreator class. Runs the 
* CoUninitialize() function to take down the context.
*/
ToastNotificationShortcutCreator::~ToastNotificationShortcutCreator()
{
	CoUninitialize();
}

/* 
* Name: CreateShortcut
* Summary: This function was inspired by the code written here: 
* https://blogs.msdn.microsoft.com/tiles_and_toasts/2015/10/16/quickstart-handling-toast-activations-from-win32-apps-in-windows-10/
* Parameters:
*	shortcutPath - The path and the name of the shortcut file.
* Return: S_OK if creating the shortcut was successfully.
*/
HRESULT ToastNotificationShortcutCreator::CreateShortcut(wchar_t *shortcutPath)
{
	if (PathFileExists(shortcutPath)) {
		return S_OK;
	}

	ComPtr<IPersistFile> persistFile;
	ComPtr<IPropertyStore> propertyStore;
	ComPtr<IShellLink> shellLink;
	HRESULT hr = S_OK;
	PROPVARIANT appIdPropVar;

	hr |= CoCreateInstance(CLSID_ShellLink, nullptr, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&shellLink));
	hr |= shellLink->SetPath(exePath);
	hr |= shellLink->SetArguments(L"");
	hr |= shellLink.As(&propertyStore);
	hr |= InitPropVariantFromString(appUserModelID, &appIdPropVar);

	if (SUCCEEDED(hr)) {
		hr = propertyStore->SetValue(PKEY_AppUserModel_ID, appIdPropVar);

		if (SUCCEEDED(hr)) {
			hr = propertyStore->Commit();

			if (SUCCEEDED(hr)) {
				hr = shellLink.As(&persistFile);

				if (SUCCEEDED(hr)) {
					hr = persistFile->Save(shortcutPath, true);
				}
			}
		}

		PropVariantClear(&appIdPropVar);
	}

	return hr;
}
