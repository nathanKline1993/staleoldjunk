﻿using System.Collections.Generic;

namespace Com.Me.Nwk25.StaleOldJunkLogic
{
    /// <summary>
    /// Contains the properties for the StaleOldJunk json config file.
    /// </summary>
    public class StaleOldJunkJsonConfigFile
    {
        /// <summary>
        /// Indicates whether or not StaleOldJunk should be applied to the Recycle Bin as well.
        /// </summary>
        public bool ShouldClearRecycleBin { get; set; }

        /// <summary>
        /// The number of days until a file is considered stale.
        /// </summary>
        public int DaysTilExpiration { get; set; }

        /// <summary>
        /// List of temporary directories where StaleOldJunk can delete files besides the Recycle Bin.
        /// </summary>
        public List<StaleOldJunkTempDirInfo> TempDirInfo { get; set; }

        /// <summary>
        /// File location where the log files will be placed.
        /// </summary>
        public string LogFileLocation { get; set; }
    }
}
