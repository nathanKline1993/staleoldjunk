﻿using Com.Me.Nwk25.UWPToWinAppUtility;
using System;
using System.IO;

namespace Com.Me.Nwk25.StaleOldJunkLogic
{
    /// <summary>
    /// Responsible for creating StaleOldJunk decorator.
    /// </summary>
    public static class StaleOldJunkDecoratorFactory
    {
        /// <summary>
        /// Creates the component that runs StaleOldJunk and decorates it with a component that create a shortcut for the toast notifications
        /// and a component for displaying the toast notification.
        /// </summary>
        /// <returns>The decorated components to run.</returns>
        public static UWPToWinAppDecorator CreateStaleOldJunkComponent()
        {
            FileInfo staleOldJunkToastXmlFileInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\" + 
                StaleOldJunkLogicResourcesTranslator.StaleOldJunkToastXmlFileLocation);
            string staleOldJunkGUIShortcutFilePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + 
                "\\Microsoft\\Windows\\Start Menu\\Programs\\StaleOldJunkGUI.lnk";

            StaleOldJunkComponent staleOldJunkComponent = new StaleOldJunkComponent(staleOldJunkToastXmlFileInfo);
            ToastNotificationDecorator toastNotificationDecorator = new ToastNotificationDecorator(staleOldJunkComponent, 
                StaleOldJunkLogicResourcesTranslator.StaleOldJunkGUIAppUserModelID, staleOldJunkToastXmlFileInfo);

            return new CreateShortcutToastNotificationDecorator(toastNotificationDecorator, 
                StaleOldJunkLogicResourcesTranslator.StaleOldJunkGUIAppUserModelID, 
                new FileInfo(StaleOldJunkLogicResourcesTranslator.StaleOldJunkGUIExeName), staleOldJunkGUIShortcutFilePath);
        }
    }
}
