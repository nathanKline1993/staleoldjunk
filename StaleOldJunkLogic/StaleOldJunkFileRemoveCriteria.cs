﻿using Com.Me.Nwk25.FileEditor;
using System;
using System.IO;

namespace Com.Me.Nwk25.StaleOldJunkLogic
{
    /// <summary>
    /// FileRemoveCriteria for StaleOldJunk. Centered around file expiration.
    /// </summary>
    public class StaleOldJunkFileRemoveCriteria : FileRemoveCriteria
    {
        #region Properties
        /// <summary>
        /// TimeSpan until a file is considered expired.
        /// </summary>
        private TimeSpan daysTilExpirations;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a TimeSpan based off the daysTilExpiration.
        /// </summary>
        /// <param name="daysTilExpiration">The number of days until the file is considered stale.</param>
        public StaleOldJunkFileRemoveCriteria(int daysTilExpiration)
        {
            this.daysTilExpirations = new TimeSpan(daysTilExpiration, 0, 0, 0);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Determines if the directory can be deleted.
        /// </summary>
        /// <param name="directoryInfo">DirectoryInfo representing the directory.</param>
        /// <returns>True if the directory is empty and stale; false otherwise.</returns>
        public override bool CanRemoveDirectory(DirectoryInfo directoryInfo)
        {
            return (base.CanRemoveDirectory(directoryInfo) && FileUtilities.IsFileOld(directoryInfo, daysTilExpirations));
        }

        /// <summary>
        /// Determines if the file can be deleted.
        /// </summary>
        /// <param name="fileInfo">FileInfo representing the file.</param>
        /// <returns>True if the file is stale; false otherwise.</returns>
        public override bool CanRemoveFile(FileInfo fileInfo)
        {
            return FileUtilities.IsFileOld(fileInfo, daysTilExpirations);
        }
        #endregion
    }
}
