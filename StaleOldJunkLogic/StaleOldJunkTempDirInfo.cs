﻿namespace Com.Me.Nwk25.StaleOldJunkLogic
{
    /// <summary>
    /// Data model for temporary directories that are monitored by StaleOldJunk.
    /// </summary>
    public class StaleOldJunkTempDirInfo
    {
        /// <summary>
        /// Indicates whether or not the temporary directory should be deleted if it is empty and old.
        /// </summary>
        public bool DeleteRoot { get; set; }

        /// <summary>
        /// Indicates the number of days till considered for the specific temporary directory.
        /// </summary>
        public int DaysTilExpiration { get; set; }

        /// <summary>
        /// Filepath of the temporary directory.
        /// </summary>
        public string Location { get; set; }
    }
}
