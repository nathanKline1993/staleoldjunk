﻿using System;

namespace Com.Me.Nwk25.StaleOldJunkLogic
{
    /// <summary>
    /// Translates the contents in the StaleOldJunkLogicResources.resx resource file to something that could be used by
    /// JsonFileFormatter, etc.
    /// </summary>
    public static class StaleOldJunkLogicResourcesTranslator
    {
        /// <summary>
        /// "StaleOldJunk"
        /// </summary>
        public static string StaleOldJunkAppName
        {
            get
            {
                return StaleOldJunkLogicResources.StaleOldJunkAppName;
            }
        }

        /// <summary>
        /// Location of the StaleOldJunk json config file.
        /// </summary>
        public static string StaleOldJunkConfigFilePath
        {
            get
            {
                return AppDomain.CurrentDomain.BaseDirectory + StaleOldJunkLogicResources.StaleOldJunkConfigFilePath;
            }
        }

        /// <summary>
        /// "StaleOldJunk.exe"
        /// </summary>
        public static string StaleOldJunkExeName
        {
            get
            {
                return StaleOldJunkLogicResources.StaleOldJunkExeName;
            }
        }

        /// <summary>
        /// Contains the AppUserModelID for toast notifications. "Com.Me.Nwk25.StaleOldJunkGUI".
        /// </summary>
        public static string StaleOldJunkGUIAppUserModelID
        {
            get
            {
                return StaleOldJunkLogicResources.StaleOldJunkGUIAppUserModelID;
            }
        }

        /// <summary>
        /// The path of the StaleOldJunkGUI executable.
        /// </summary>
        public static string StaleOldJunkGUIExeName
        {
            get
            {
                return StaleOldJunkLogicResources.StaleOldJunkGUIExeName;
            }
        }

        /// <summary>
        /// Contains the introduction line for the log file. The introduction line is for each directory that StaleOldJunk cleaned. After the
        /// introduction line the stale files and directories that were deleted are listed.
        /// </summary>
        public static string StaleOldJunkRemovalIntroductionFormatString
        {
            get
            {
                return StaleOldJunkLogicResources.StaleOldJunkRemovalIntroductionFormatString;
            }
        }

        /// <summary>
        /// Description for the StaleOldJunk task in the task scheduler.
        /// </summary>
        public static string StaleOldJunkTaskDesc
        {
            get
            {
                return StaleOldJunkLogicResources.StaleOldJunkTaskDesc;
            }
        }

        /// <summary>
        /// Name of the StaleOldJunk task in the task scheduler. 
        /// </summary>
        public static string StaleOldJunkTaskName
        {
            get
            {
                return StaleOldJunkLogicResources.StaleOldJunkTaskName;
            }
        }

        /// <summary>
        /// The message for the toast notification if files were deleted but no log file location was set. Does not contain XML.
        /// </summary>
        public static string StaleOldJunkToastMessageDeleteFile
        {
            get
            {
                return StaleOldJunkLogicResources.StaleOldJunkToastMessageDeleteFile;
            }
        }

        /// <summary>
        /// The message for the toast notification if files were deleted and a log file location was set. Does not contain XML.
        /// </summary>
        public static string StaleOldJunkToastMessageDeleteFileDetailsInLogFile
        {
            get
            {
                return StaleOldJunkLogicResources.StaleOldJunkToastMessageDeleteFileDetailsInLogFile;
            }
        }

        /// <summary>
        /// The message for the toast notification if files were not deleted. Does not contain XML.
        /// </summary>
        public static string StaleOldJunkToastMessageDidNotDeleteFile
        {
            get
            {
                return StaleOldJunkLogicResources.StaleOldJunkToastMessageDidNotDeleteFile;
            }
        }

        /// <summary>
        /// String containing the location of the file that contains the XML for the toast notification.
        /// </summary>
        public static string StaleOldJunkToastXmlFileLocation
        {
            get
            {
                return StaleOldJunkLogicResources.StaleOldJunkToastXmlFileLocation;
            }
        }
    }
}
