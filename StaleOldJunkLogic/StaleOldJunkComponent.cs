﻿using Com.Me.Nwk25.FileEditor;
using Com.Me.Nwk25.UWPToWinAppUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Management.Automation;
using System.Text;
using Windows.Data.Xml.Dom;

namespace Com.Me.Nwk25.StaleOldJunkLogic
{
    /// <summary>
    /// Meant to be the central component for StaleOldJunk decorator pattern.
    /// </summary>
    public class StaleOldJunkComponent : IUWPToWinAppBuildable
    {
        private FileInfo toastNotificationXmlFileInfo = null;

        /// <summary>
        /// Constructor for StaleOldJunkComponent.
        /// </summary>
        /// <param name="toastNotificationXmlFileInfo">Will contain the message for the toast notification.</param>
        public StaleOldJunkComponent(FileInfo toastNotificationXmlFileInfo)
        {
            this.toastNotificationXmlFileInfo = toastNotificationXmlFileInfo;
        }

        /// <summary>
        /// Deletes all the stale files in the locations specified by the staleoldjunk.config.json file. Reports all of the files that were deleted in the
        /// log file specified in the config file. Creates the toast notification xml file.
        /// </summary>
        /// <returns>True if successful; false otherwise.</returns>
        public bool PerformComponentOperation()
        {
            StaleOldJunkJsonConfigFile staleOldJunkJsonConfig = null;
            StringBuilder staleOldJunkFileRemovalRecorder = new StringBuilder();
            XmlDocument toastMessageXmlDocument;

            if (!JsonFileFormatter.Read<StaleOldJunkJsonConfigFile>(StaleOldJunkLogicResourcesTranslator.StaleOldJunkConfigFilePath, out staleOldJunkJsonConfig))
            {
                return false;
            }

            if (staleOldJunkJsonConfig.ShouldClearRecycleBin)
            {
                RunClear_StaleOldJunkInRecycleBin(staleOldJunkJsonConfig.DaysTilExpiration, staleOldJunkFileRemovalRecorder);
            }

            if (staleOldJunkJsonConfig.TempDirInfo != null)
            {
                CleanTempDirectories(staleOldJunkJsonConfig.TempDirInfo, staleOldJunkFileRemovalRecorder);
            }

            string logFileLocation = staleOldJunkJsonConfig.LogFileLocation;

            if (!String.IsNullOrWhiteSpace(staleOldJunkFileRemovalRecorder.ToString()) && !String.IsNullOrWhiteSpace(logFileLocation) 
                && Directory.Exists(logFileLocation))
            {
                StoreRecorderInfoInLogFile(logFileLocation, staleOldJunkFileRemovalRecorder);
                toastMessageXmlDocument = ToastNotificationXmlCreator.CreateSimpleToastNotificationText(
                    StaleOldJunkLogicResourcesTranslator.StaleOldJunkToastMessageDeleteFileDetailsInLogFile);
            } else if (!String.IsNullOrWhiteSpace(staleOldJunkFileRemovalRecorder.ToString()))
            {
                toastMessageXmlDocument = ToastNotificationXmlCreator.CreateSimpleToastNotificationText(
                    StaleOldJunkLogicResourcesTranslator.StaleOldJunkToastMessageDeleteFile);
            } else
            {
                toastMessageXmlDocument = ToastNotificationXmlCreator.CreateSimpleToastNotificationText(
                    StaleOldJunkLogicResourcesTranslator.StaleOldJunkToastMessageDidNotDeleteFile);
            }

            File.WriteAllText(toastNotificationXmlFileInfo.FullName, toastMessageXmlDocument.GetXml());

            return true;
        }

        /// <summary>
        /// Removes stale old files and directories from the temporary directories. Also records the names of files and directories that have been deleted
        /// using a StringBuilder.
        /// </summary>
        /// <param name="tempDirInfoList">List of StaleOldJunkTempDirInfo for each temporary directory mentioned in the config file.</param>
        /// <param name="staleOldJunkFileRemovalRecorder">StringBuilder recording the removed file and directory names.</param>
        private static void CleanTempDirectories(List<StaleOldJunkTempDirInfo> tempDirInfoList, StringBuilder staleOldJunkFileRemovalRecorder)
        {
            List<string> listOfFileNamesRemoved = new List<string>();

            foreach (StaleOldJunkTempDirInfo tempDirInfo in tempDirInfoList)
            {
                if (!Directory.Exists(tempDirInfo.Location))
                {
                    continue;
                }

                DirectoryInfo directory = new DirectoryInfo(tempDirInfo.Location);
                FileRemover.RemoveFileBasedOnCriteria(directory, new StaleOldJunkFileRemoveCriteria(tempDirInfo.DaysTilExpiration), tempDirInfo.DeleteRoot, 
                    listOfFileNamesRemoved);

                if (listOfFileNamesRemoved.Count > 0)
                {
                    StoreStaleOldJunkFileRemovalTextInRecorder(staleOldJunkFileRemovalRecorder, listOfFileNamesRemoved, directory.Name);
                    listOfFileNamesRemoved.Clear();
                }
            }
        }

        /// <summary>
        /// Runs the Powershell command Clear-StaleOldJunkInRecycleBin. Records the files and directories that have been removed by the 
        /// Clear-StaleOldJunkInRecycleBin command.
        /// </summary>
        /// <param name="daysTilExpiration">The number of days that are considered stale old junk.</param>
        /// <param name="staleOldJunkFileRemovalRecorder">StringBuilder recording the removed file and directory names.</param>
        private static void RunClear_StaleOldJunkInRecycleBin(int daysTilExpiration, StringBuilder staleOldJunkFileRemovalRecorder)
        {
            List<string> listOfFileNamesRemoved = new List<string>();
            PowerShell shell = PowerShell.Create();

            shell.AddCommand("Set-ExecutionPolicy");
            shell.AddParameter("ExecutionPolicy", "RemoteSigned");
            shell.AddParameter("Scope", "Process");
            shell.AddParameter("Force");

            /* Don't remove; this is necessary. For some reason Set-ExecutionPolicy doesn't run before Import-Module.
             * There might be something I don't know about the PowerShell class's Command queue. */
            shell.Invoke();

            shell.AddCommand("Import-Module");
            shell.AddArgument("StaleOldJunkRecycleBin");

            shell.AddCommand("Clear-StaleOldJunkInRecycleBin");
            shell.AddParameter("DaysTilExpiration", daysTilExpiration);
            shell.AddParameter("Verbose");

            foreach (PSObject psObject in shell.Invoke())
            {
                listOfFileNamesRemoved.Add(psObject.ToString());
            }

            if (listOfFileNamesRemoved.Count > 0)
            {
                StoreStaleOldJunkFileRemovalTextInRecorder(staleOldJunkFileRemovalRecorder, listOfFileNamesRemoved, "Recycle Bin");
            }
        }

        /// <summary>
        /// Creates a log file and places the contents of the StringBuilder, staleOldJunkFileRemovalRecorder, in the log file.
        /// </summary>
        /// <param name="logFileLocation">File location where the log file will be placed.</param>
        /// <param name="staleOldJunkFileRemovalRecorder">Contains the names of the removed files and directories.</param>
        private static void StoreRecorderInfoInLogFile(string logFileLocation, StringBuilder staleOldJunkFileRemovalRecorder)
        {
            FileNameGenerator logFileNameGenerator = new DatedLogFileNameGenerator(StaleOldJunkLogicResourcesTranslator.StaleOldJunkAppName);

            DirectoryInfo logFileDirectoryInfo = new DirectoryInfo(logFileLocation);
            FileRecordFiler.FileTextInDirectory(staleOldJunkFileRemovalRecorder.ToString(), logFileDirectoryInfo, logFileNameGenerator);
        }

        /// <summary>
        /// Places the list of files and directories removed in the StringBuilder. There placed in a specific format.
        /// </summary>
        /// <param name="fileRemoveRecorder">StringBuilder recording the removed file and directory names.</param>
        /// <param name="listOfFileNamesRemoved">Name of files and directories removed.</param>
        /// <param name="directoryName">Name of the directory where the removal occurred.</param>
        private static void StoreStaleOldJunkFileRemovalTextInRecorder(StringBuilder fileRemoveRecorder, List<string> listOfFileNamesRemoved, 
            string directoryName)
        {
            fileRemoveRecorder.AppendFormat(StaleOldJunkLogicResourcesTranslator.StaleOldJunkRemovalIntroductionFormatString + Environment.NewLine,
                directoryName);

            foreach (string fileName in listOfFileNamesRemoved)
            {
                fileRemoveRecorder.AppendLine(fileName);
            }
        }
    }
}
