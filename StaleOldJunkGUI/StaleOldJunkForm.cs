﻿using Com.Me.Nwk25.FileEditor;
using Com.Me.Nwk25.StaleOldJunkLogic;
using Microsoft.Win32.TaskScheduler;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace Com.Me.Nwk25.StaleOldJunkGUI
{
    /// <summary>
    /// Contains event handlers for the various controls on the StaleOldJunkForm.
    /// </summary>
    public partial class StaleOldJunkForm : Form
    {
        private StaleOldJunkJsonConfigFile staleOldJunkJsonConfig = null;

        /// <summary>
        /// Initializes the components or controls.
        /// </summary>
        public StaleOldJunkForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The "Add" button's click event handler. Displays a FolderBrowserDialog and if the user selects a folder and clicks ok 
        /// places that folder in the list temporary directory list and adds that temporary directory to a list with default 
        /// values.
        /// </summary>
        /// <param name="sender">The control that experiences the event.</param>
        /// <param name="e">The event itself.</param>
        private void addButton_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
            {
                folderBrowserDialog.Description = StaleOldJunkGUI.Properties.Resources.AddTempDirFolderDialogDescription;

                if (showAndValidateFolderBrowserDialog(folderBrowserDialog))
                {
                    StaleOldJunkTempDirInfo staleOldJunkTempDirInfo = new StaleOldJunkTempDirInfo
                    {
                        Location = folderBrowserDialog.SelectedPath,
                        DaysTilExpiration = 30,
                        DeleteRoot = false
                    };

                    tempDirInfoBindingSource.Add(staleOldJunkTempDirInfo);
                }
            }
        }

        /// <summary>
        /// Event handler for the "Create Scheduled Task" button. Creates scheduled task for StaleOldJunk that will run every day.
        /// "Create Scheduled Task" button should then be replaced with the "Remove Scheduled Task" button.
        /// </summary>
        /// <param name="sender">The control that experiences the event.</param>
        /// <param name="e">The event itself.</param>
        private void createScheduledTaskButton_Click(object sender, EventArgs e)
        {
            if (!isStaleOldJunkTaskInScheduleTaskRoot())
            {
                using (TaskService taskService = new TaskService())
                {
                    TaskDefinition staleOldJunkTaskDef = taskService.NewTask();
                    staleOldJunkTaskDef.RegistrationInfo.Description = StaleOldJunkLogicResourcesTranslator.StaleOldJunkTaskDesc;

                    staleOldJunkTaskDef.Triggers.Add(new DailyTrigger { DaysInterval = 1 });

                    staleOldJunkTaskDef.Actions.Add(new ExecAction(Directory.GetCurrentDirectory() + "\\" + StaleOldJunkLogicResourcesTranslator.StaleOldJunkExeName));

                    staleOldJunkTaskDef.Settings.IdleSettings.StopOnIdleEnd = false;
                    staleOldJunkTaskDef.Settings.StartWhenAvailable = true;

                    taskService.RootFolder.RegisterTaskDefinition(StaleOldJunkLogicResourcesTranslator.StaleOldJunkTaskName, staleOldJunkTaskDef);
                }
            }

            switchScheduledTaskButtonsVisibility();
        }

        /// <summary>
        /// Event handler for the "Exit" button.
        /// </summary>
        /// <param name="sender">The control that experiences the event.</param>
        /// <param name="e">The event itself.</param>
        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Data binding is not updated for some controls when a button is clicked a certain way (alt + some key).
        /// This updates the data binding no matter what.
        /// </summary>
        private void forceOnPropertyChangedEventUsingFocus()
        {
            daysTilExpirationNumUpDown.Focus();
        }

        /// <summary>
        /// Determines if the StaleOldJunk task is in the scheduled task root folder.
        /// </summary>
        /// <returns>True if the StaleOldJunk task exist in the root folder, false otherwise.</returns>
        private bool isStaleOldJunkTaskInScheduleTaskRoot()
        {
            Task staleOldJunkTask = null;

            using (TaskService taskService = new TaskService())
            {
                staleOldJunkTask = taskService.FindTask(StaleOldJunkLogicResourcesTranslator.StaleOldJunkTaskName);
            }

            return (staleOldJunkTask != null);
        }

        /// <summary>
        /// Looks in the log file location if it exists, counts the number log files, and updates the logFileCountFieldLabel
        /// with the number of the log files in the log file directory.
        /// </summary>
        private void loadLogFileCount()
        {
            string logFileDirectoryPath = logFileLocationTextBox.Text;

            if (!String.IsNullOrWhiteSpace(logFileDirectoryPath) && Directory.Exists(logFileDirectoryPath))
            {
                DirectoryInfo logFileDirectoryInfo = new DirectoryInfo(logFileDirectoryPath);
                logFileCountFieldLabel.Text = String.Format("{0}", DirectoryUtilities.FileCountInDirectory(logFileDirectoryInfo));
            } else
            {
                logFileCountFieldLabel.Text = String.Empty;
            }
        }

        /// <summary>
        /// Opens the FolderBrowserDialog and saves the log file location you selected in the dialog. The log file count is
        /// updated.
        /// </summary>
        /// <param name="sender">The control that experiences the event.</param>
        /// <param name="e">The event itself.</param>
        private void logFileLocationButton_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
            {
                folderBrowserDialog.Description = Properties.Resources.SetDesktopAlertLogFileLocationDescription;
                
                if (showAndValidateFolderBrowserDialog(folderBrowserDialog))
                {
                    logFileLocationTextBox.Text = folderBrowserDialog.SelectedPath;
                }
            }

            loadLogFileCount();
        }

        /// <summary>
        /// Runs toggleOpenLogFileButton().
        /// </summary>
        /// <param name="sender">The control that experiences the event.</param>
        /// <param name="e">The event itself.</param>
        private void logFileLocationTextBox_TextChanged(object sender, EventArgs e)
        {
            toggleOpenLogFileButton();
        }

        /// <summary>
        /// Shows an OpenFileDialog with the initial directory set to the log file location. Opens the selected file in
        /// the default application.
        /// </summary>
        /// <param name="sender">The control that experiences the event.</param>
        /// <param name="e">The event itself.</param>
        private void openLogFilesButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = logFileLocationTextBox.Text;
                openFileDialog.Multiselect = false;

                DialogResult dialogResult = openFileDialog.ShowDialog();

                if (dialogResult == DialogResult.OK && File.Exists(openFileDialog.FileName))
                {
                    Process.Start(openFileDialog.FileName);
                }
            }
        }

        /// <summary>
        /// The "Remove" button's click event handler. Removes the selected temporary directory from the config file and list.
        /// </summary>
        /// <param name="sender">The control that experiences the event.</param>
        /// <param name="e">The event itself.</param>
        private void removeButton_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection selectedRowCollection = tempDirInfoGridView.SelectedRows;

            foreach (DataGridViewRow row in selectedRowCollection)
            {
                tempDirInfoBindingSource.Remove(row.DataBoundItem);
            }
        }

        /// <summary>
        /// Event handler for the "Remove Scheduled Task" button. Removes scheduled task for StaleOldJunk.
        /// "Remove Scheduled Task" button should then be replaced with the "Create Scheduled Task" button.
        /// </summary>
        /// <param name="sender">The control that experiences the event.</param>
        /// <param name="e">The event itself.</param>
        private void removeScheduledTaskButton_Click(object sender, EventArgs e)
        {
            if (isStaleOldJunkTaskInScheduleTaskRoot())
            {
                using (TaskService taskService = new TaskService())
                {
                    taskService.RootFolder.DeleteTask(StaleOldJunkLogicResourcesTranslator.StaleOldJunkTaskName);
                }
            }

            switchScheduledTaskButtonsVisibility();
        }

        /// <summary>
        /// Forces runs the StaleOldJunk executable. Also saves the configuration.
        /// </summary>
        /// <param name="sender">The control that experiences the event.</param>
        /// <param name="e">The event itself.</param>
        private void runButton_Click(object sender, EventArgs e)
        {
            saveStaleOldJunkConfigFile();

            using (Process staleOldJunkProcess = Process.Start(StaleOldJunkLogicResourcesTranslator.StaleOldJunkExeName))
            {
                staleOldJunkProcess.WaitForExit();
            }

            loadLogFileCount();
        }

        /// <summary>
        /// Forces the OnPropertyChangedEvent to occur for certain controls and then writes to the config file.
        /// </summary>
        private void saveStaleOldJunkConfigFile()
        {
            forceOnPropertyChangedEventUsingFocus();

            JsonFileFormatter.Write<StaleOldJunkJsonConfigFile>(StaleOldJunkLogicResourcesTranslator.StaleOldJunkConfigFilePath, staleOldJunkJsonConfig);
        }

        /// <summary>
        /// Shows the FolderBrowserDialog and validates that the dialog returned DialogResult.OK status and there
        /// was an actually selected path.
        /// </summary>
        /// <param name="folderBrowserDialog">FolderBrowserDialog to be shown and validated.</param>
        /// <returns>True if the DialogResult is OK and selected path is not null or whitespace. False otherwise.</returns>
        private bool showAndValidateFolderBrowserDialog(FolderBrowserDialog folderBrowserDialog)
        {
            DialogResult result = folderBrowserDialog.ShowDialog();

            return result == DialogResult.OK && !String.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath);
        }

        /// <summary>
        /// Saves the config file as the GUI closes.
        /// </summary>
        /// <param name="sender">The control that experiences the event.</param>
        /// <param name="e">The event itself.</param>
        private void StaleOldJunkForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveStaleOldJunkConfigFile();
        }

        /// <summary>
        /// As the GUI loads establishes data binding and shows the correct "Scheduled Task" button.
        /// </summary>
        /// <param name="sender">The control that experiences the event.</param>
        /// <param name="e">The event itself.</param>
        private void StaleOldJunkForm_Load(object sender, EventArgs e)
        {
            if (!JsonFileFormatter.Read<StaleOldJunkJsonConfigFile>(StaleOldJunkLogicResourcesTranslator.StaleOldJunkConfigFilePath, out staleOldJunkJsonConfig))
            {
                staleOldJunkJsonConfig = new StaleOldJunkJsonConfigFile();
            }

            if (staleOldJunkJsonConfig.TempDirInfo == null)
            {
                staleOldJunkJsonConfig.TempDirInfo = new List<StaleOldJunkTempDirInfo>();
            }

            staleOldJunkJsonConfigFileBindingSource.Add(staleOldJunkJsonConfig);

            toggleOpenLogFileButton();
            switchScheduledTaskButtonsVisibility();
            loadLogFileCount();
        }

        /// <summary>
        /// Shows the correct "Scheduled Task" button given the circumstance.
        /// </summary>
        private void switchScheduledTaskButtonsVisibility()
        {
            createScheduledTaskButton.Visible = !isStaleOldJunkTaskInScheduleTaskRoot();
            removeScheduledTaskButton.Visible = !createScheduledTaskButton.Visible;
        }

        /// <summary>
        /// Enables the openLogFilesButton if the log file location exist.
        /// </summary>
        private void toggleOpenLogFileButton()
        {
            openLogFilesButton.Enabled = (!String.IsNullOrWhiteSpace(logFileLocationTextBox.Text) && Directory.Exists(logFileLocationTextBox.Text));
        }
    }
}
