﻿namespace Com.Me.Nwk25.StaleOldJunkGUI
{
    partial class StaleOldJunkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dayTilExpirationLabel = new System.Windows.Forms.Label();
            this.daysTilExpirationNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.cleanRecycleBinChkBox = new System.Windows.Forms.CheckBox();
            this.runButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.additionalOptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.removeScheduledTaskButton = new System.Windows.Forms.Button();
            this.createScheduledTaskButton = new System.Windows.Forms.Button();
            this.tempDirInfoGridView = new System.Windows.Forms.DataGridView();
            this.locationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deleteRootDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tempDirInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tempDirInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.removeButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.openLogFilesButton = new System.Windows.Forms.Button();
            this.logFileLocationButton = new System.Windows.Forms.Button();
            this.logFileLocationTextBox = new System.Windows.Forms.TextBox();
            this.logFileLocationLabel = new System.Windows.Forms.Label();
            this.logFileCountLabel = new System.Windows.Forms.Label();
            this.logFileCountFieldLabel = new System.Windows.Forms.Label();
            this.daysTilExpirationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staleOldJunkJsonConfigFileBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.daysTilExpirationNumUpDown)).BeginInit();
            this.additionalOptionsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tempDirInfoGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempDirInfoBindingSource)).BeginInit();
            this.tempDirInfoGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.staleOldJunkJsonConfigFileBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dayTilExpirationLabel
            // 
            this.dayTilExpirationLabel.AutoSize = true;
            this.dayTilExpirationLabel.Location = new System.Drawing.Point(13, 13);
            this.dayTilExpirationLabel.Name = "dayTilExpirationLabel";
            this.dayTilExpirationLabel.Size = new System.Drawing.Size(97, 13);
            this.dayTilExpirationLabel.TabIndex = 0;
            this.dayTilExpirationLabel.Text = "Days Til Expiration:";
            // 
            // daysTilExpirationNumUpDown
            // 
            this.daysTilExpirationNumUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.staleOldJunkJsonConfigFileBindingSource, "DaysTilExpiration", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.daysTilExpirationNumUpDown.Location = new System.Drawing.Point(116, 11);
            this.daysTilExpirationNumUpDown.Name = "daysTilExpirationNumUpDown";
            this.daysTilExpirationNumUpDown.Size = new System.Drawing.Size(120, 20);
            this.daysTilExpirationNumUpDown.TabIndex = 1;
            // 
            // cleanRecycleBinChkBox
            // 
            this.cleanRecycleBinChkBox.AutoSize = true;
            this.cleanRecycleBinChkBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.staleOldJunkJsonConfigFileBindingSource, "ShouldClearRecycleBin", true));
            this.cleanRecycleBinChkBox.Location = new System.Drawing.Point(6, 19);
            this.cleanRecycleBinChkBox.Name = "cleanRecycleBinChkBox";
            this.cleanRecycleBinChkBox.Size = new System.Drawing.Size(121, 17);
            this.cleanRecycleBinChkBox.TabIndex = 2;
            this.cleanRecycleBinChkBox.Text = "Apply to RecycleBin";
            this.cleanRecycleBinChkBox.UseVisualStyleBackColor = true;
            // 
            // runButton
            // 
            this.runButton.Location = new System.Drawing.Point(259, 358);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(75, 23);
            this.runButton.TabIndex = 3;
            this.runButton.Text = "&Run";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(340, 358);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 5;
            this.exitButton.Text = "E&xit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // additionalOptionsGroupBox
            // 
            this.additionalOptionsGroupBox.Controls.Add(this.logFileCountLabel);
            this.additionalOptionsGroupBox.Controls.Add(this.logFileCountFieldLabel);
            this.additionalOptionsGroupBox.Controls.Add(this.openLogFilesButton);
            this.additionalOptionsGroupBox.Controls.Add(this.removeScheduledTaskButton);
            this.additionalOptionsGroupBox.Controls.Add(this.logFileLocationButton);
            this.additionalOptionsGroupBox.Controls.Add(this.createScheduledTaskButton);
            this.additionalOptionsGroupBox.Controls.Add(this.cleanRecycleBinChkBox);
            this.additionalOptionsGroupBox.Controls.Add(this.logFileLocationTextBox);
            this.additionalOptionsGroupBox.Controls.Add(this.logFileLocationLabel);
            this.additionalOptionsGroupBox.Location = new System.Drawing.Point(16, 207);
            this.additionalOptionsGroupBox.Name = "additionalOptionsGroupBox";
            this.additionalOptionsGroupBox.Size = new System.Drawing.Size(399, 145);
            this.additionalOptionsGroupBox.TabIndex = 6;
            this.additionalOptionsGroupBox.TabStop = false;
            this.additionalOptionsGroupBox.Text = "Additional Options";
            // 
            // removeScheduledTaskButton
            // 
            this.removeScheduledTaskButton.Location = new System.Drawing.Point(6, 42);
            this.removeScheduledTaskButton.Name = "removeScheduledTaskButton";
            this.removeScheduledTaskButton.Size = new System.Drawing.Size(152, 23);
            this.removeScheduledTaskButton.TabIndex = 4;
            this.removeScheduledTaskButton.Text = "Remove Scheduled Task";
            this.removeScheduledTaskButton.UseVisualStyleBackColor = true;
            this.removeScheduledTaskButton.Visible = false;
            this.removeScheduledTaskButton.Click += new System.EventHandler(this.removeScheduledTaskButton_Click);
            // 
            // createScheduledTaskButton
            // 
            this.createScheduledTaskButton.Location = new System.Drawing.Point(6, 42);
            this.createScheduledTaskButton.Name = "createScheduledTaskButton";
            this.createScheduledTaskButton.Size = new System.Drawing.Size(152, 23);
            this.createScheduledTaskButton.TabIndex = 3;
            this.createScheduledTaskButton.Text = "Create Scheduled Task";
            this.createScheduledTaskButton.UseVisualStyleBackColor = true;
            this.createScheduledTaskButton.Visible = false;
            this.createScheduledTaskButton.Click += new System.EventHandler(this.createScheduledTaskButton_Click);
            // 
            // tempDirInfoGridView
            // 
            this.tempDirInfoGridView.AllowUserToAddRows = false;
            this.tempDirInfoGridView.AutoGenerateColumns = false;
            this.tempDirInfoGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tempDirInfoGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.locationDataGridViewTextBoxColumn,
            this.daysTilExpirationDataGridViewTextBoxColumn,
            this.deleteRootDataGridViewCheckBoxColumn});
            this.tempDirInfoGridView.DataSource = this.tempDirInfoBindingSource;
            this.tempDirInfoGridView.Location = new System.Drawing.Point(6, 19);
            this.tempDirInfoGridView.Name = "tempDirInfoGridView";
            this.tempDirInfoGridView.Size = new System.Drawing.Size(304, 133);
            this.tempDirInfoGridView.TabIndex = 7;
            // 
            // locationDataGridViewTextBoxColumn
            // 
            this.locationDataGridViewTextBoxColumn.DataPropertyName = "Location";
            this.locationDataGridViewTextBoxColumn.HeaderText = "Location";
            this.locationDataGridViewTextBoxColumn.Name = "locationDataGridViewTextBoxColumn";
            this.locationDataGridViewTextBoxColumn.ReadOnly = true;
            this.locationDataGridViewTextBoxColumn.Width = 60;
            // 
            // deleteRootDataGridViewCheckBoxColumn
            // 
            this.deleteRootDataGridViewCheckBoxColumn.DataPropertyName = "DeleteRoot";
            this.deleteRootDataGridViewCheckBoxColumn.HeaderText = "Delete Root?";
            this.deleteRootDataGridViewCheckBoxColumn.Name = "deleteRootDataGridViewCheckBoxColumn";
            this.deleteRootDataGridViewCheckBoxColumn.Width = 80;
            // 
            // tempDirInfoBindingSource
            // 
            this.tempDirInfoBindingSource.DataMember = "TempDirInfo";
            this.tempDirInfoBindingSource.DataSource = this.staleOldJunkJsonConfigFileBindingSource;
            // 
            // tempDirInfoGroupBox
            // 
            this.tempDirInfoGroupBox.Controls.Add(this.removeButton);
            this.tempDirInfoGroupBox.Controls.Add(this.addButton);
            this.tempDirInfoGroupBox.Controls.Add(this.tempDirInfoGridView);
            this.tempDirInfoGroupBox.Location = new System.Drawing.Point(16, 37);
            this.tempDirInfoGroupBox.Name = "tempDirInfoGroupBox";
            this.tempDirInfoGroupBox.Size = new System.Drawing.Size(399, 164);
            this.tempDirInfoGroupBox.TabIndex = 8;
            this.tempDirInfoGroupBox.TabStop = false;
            this.tempDirInfoGroupBox.Text = "Temporary Directories";
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(317, 50);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 9;
            this.removeButton.Text = "R&emove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(317, 20);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 8;
            this.addButton.Text = "&Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // openLogFilesButton
            // 
            this.openLogFilesButton.Location = new System.Drawing.Point(103, 98);
            this.openLogFilesButton.Name = "openLogFilesButton";
            this.openLogFilesButton.Size = new System.Drawing.Size(87, 23);
            this.openLogFilesButton.TabIndex = 8;
            this.openLogFilesButton.Text = "&Open Log Files";
            this.openLogFilesButton.UseVisualStyleBackColor = true;
            this.openLogFilesButton.Click += new System.EventHandler(this.openLogFilesButton_Click);
            // 
            // logFileLocationButton
            // 
            this.logFileLocationButton.Location = new System.Drawing.Point(355, 70);
            this.logFileLocationButton.Name = "logFileLocationButton";
            this.logFileLocationButton.Size = new System.Drawing.Size(25, 23);
            this.logFileLocationButton.TabIndex = 7;
            this.logFileLocationButton.Text = "...";
            this.logFileLocationButton.UseVisualStyleBackColor = true;
            this.logFileLocationButton.Click += new System.EventHandler(this.logFileLocationButton_Click);
            // 
            // logFileLocationTextBox
            // 
            this.logFileLocationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.staleOldJunkJsonConfigFileBindingSource, "LogFileLocation", true));
            this.logFileLocationTextBox.Location = new System.Drawing.Point(103, 72);
            this.logFileLocationTextBox.Name = "logFileLocationTextBox";
            this.logFileLocationTextBox.ReadOnly = true;
            this.logFileLocationTextBox.Size = new System.Drawing.Size(246, 20);
            this.logFileLocationTextBox.TabIndex = 6;
            this.logFileLocationTextBox.TextChanged += new System.EventHandler(this.logFileLocationTextBox_TextChanged);
            // 
            // logFileLocationLabel
            // 
            this.logFileLocationLabel.AutoSize = true;
            this.logFileLocationLabel.Location = new System.Drawing.Point(6, 75);
            this.logFileLocationLabel.Name = "logFileLocationLabel";
            this.logFileLocationLabel.Size = new System.Drawing.Size(91, 13);
            this.logFileLocationLabel.TabIndex = 5;
            this.logFileLocationLabel.Text = "Log File Location:";
            // 
            // logFileCountLabel
            // 
            this.logFileCountLabel.AutoSize = true;
            this.logFileCountLabel.Location = new System.Drawing.Point(16, 124);
            this.logFileCountLabel.Name = "logFileCountLabel";
            this.logFileCountLabel.Size = new System.Drawing.Size(78, 13);
            this.logFileCountLabel.TabIndex = 9;
            this.logFileCountLabel.Text = "Log File Count:";
            // 
            // logFileCountFieldLabel
            // 
            this.logFileCountFieldLabel.AutoSize = true;
            this.logFileCountFieldLabel.Location = new System.Drawing.Point(100, 124);
            this.logFileCountFieldLabel.Name = "logFileCountFieldLabel";
            this.logFileCountFieldLabel.Size = new System.Drawing.Size(0, 13);
            this.logFileCountFieldLabel.TabIndex = 9;
            // 
            // daysTilExpirationDataGridViewTextBoxColumn
            // 
            this.daysTilExpirationDataGridViewTextBoxColumn.DataPropertyName = "DaysTilExpiration";
            this.daysTilExpirationDataGridViewTextBoxColumn.HeaderText = "Days Til Expiration";
            this.daysTilExpirationDataGridViewTextBoxColumn.Name = "daysTilExpirationDataGridViewTextBoxColumn";
            this.daysTilExpirationDataGridViewTextBoxColumn.Width = 120;
            // 
            // staleOldJunkJsonConfigFileBindingSource
            // 
            this.staleOldJunkJsonConfigFileBindingSource.DataSource = typeof(Com.Me.Nwk25.StaleOldJunkLogic.StaleOldJunkJsonConfigFile);
            // 
            // StaleOldJunkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 389);
            this.Controls.Add(this.tempDirInfoGroupBox);
            this.Controls.Add(this.additionalOptionsGroupBox);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.daysTilExpirationNumUpDown);
            this.Controls.Add(this.dayTilExpirationLabel);
            this.Name = "StaleOldJunkForm";
            this.Text = "StaleOldJunk";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StaleOldJunkForm_FormClosing);
            this.Load += new System.EventHandler(this.StaleOldJunkForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.daysTilExpirationNumUpDown)).EndInit();
            this.additionalOptionsGroupBox.ResumeLayout(false);
            this.additionalOptionsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tempDirInfoGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempDirInfoBindingSource)).EndInit();
            this.tempDirInfoGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.staleOldJunkJsonConfigFileBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label dayTilExpirationLabel;
        private System.Windows.Forms.NumericUpDown daysTilExpirationNumUpDown;
        private System.Windows.Forms.CheckBox cleanRecycleBinChkBox;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.BindingSource staleOldJunkJsonConfigFileBindingSource;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.GroupBox additionalOptionsGroupBox;
        private System.Windows.Forms.Button removeScheduledTaskButton;
        private System.Windows.Forms.Button createScheduledTaskButton;
        private System.Windows.Forms.DataGridView tempDirInfoGridView;
        private System.Windows.Forms.BindingSource tempDirInfoBindingSource;
        private System.Windows.Forms.GroupBox tempDirInfoGroupBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn locationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn daysTilExpirationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn deleteRootDataGridViewCheckBoxColumn;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button logFileLocationButton;
        private System.Windows.Forms.TextBox logFileLocationTextBox;
        private System.Windows.Forms.Label logFileLocationLabel;
        private System.Windows.Forms.Button openLogFilesButton;
        private System.Windows.Forms.Label logFileCountLabel;
        private System.Windows.Forms.Label logFileCountFieldLabel;
    }
}

