﻿using System;
using System.Windows.Forms;

namespace Com.Me.Nwk25.StaleOldJunkGUI
{
    /// <summary>
    /// Main class for StaleOldJunkGUI.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new StaleOldJunkForm());
        }
    }
}
