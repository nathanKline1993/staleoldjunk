﻿#
# StaleOldJunkRecycleBin.tests.ps1
#

Describe "StaleOldJunkRecycleBinTest" {
	Import-Module StaleOldJunkRecycleBin

	Context "Exists" {
		It "runs" {
			Clear-StaleOldJunkInRecycleBin -DaysTilExpiration 45 | Should Be
		}
	}

	Context "Deletes files and directories that apply and prints out name if Verbose is specified" {
		$shell = New-Object -ComObject "Shell.Application"
		$Recycler = $shell.Namespace(0xa)
		$daysTilExpiration = 45

		Mock Write-Output {} -ModuleName StaleOldJunkRecycleBin
		Mock Remove-Item {} -ModuleName StaleOldJunkRecycleBin
		Mock New-TimeSpan { return @{ Days = $daysTilExpiration + 1; } } -ModuleName StaleOldJunkRecycleBin

		Clear-StaleOldJunkInRecycleBin -DaysTilExpiration $daysTilExpiration

		It "attempts to remove files in RecycleBin" {
			Assert-MockCalled Remove-Item -ModuleName StaleOldJunkRecycleBin
		}

		It "did not print file names since Verbose was never passed through" {
			Assert-MockCalled Write-Output -Times 0 -Exactly -ModuleName StaleOldJunkRecycleBin
		}

		Clear-StaleOldJunkInRecycleBin -DaysTilExpiration $daysTilExpiration -Verbose

		It "did print file names when Verbose was passed" {
			Assert-MockCalled Write-Output -ModuleName StaleOldJunkRecycleBin
		}
	}
}