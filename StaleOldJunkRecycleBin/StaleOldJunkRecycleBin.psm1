#
# StaleOldJunkRecycleBin.psm1
#
# Inspired by Baldwin D. at http://baldwin-ps.blogspot.be/2013/07/empty-recycle-bin-with-retention-time.html
#

Function Clear-StaleOldJunkInRecycleBin {
	[CmdletBinding()]
	Param(
		[Parameter(Mandatory=$True)]
		[int]$DaysTilExpiration
	)

	$Shell = New-Object -ComObject Shell.Application
	$Recycler = $Shell.NameSpace(0xa)

	foreach ($item in $Recycler.Items()) {
		$deletedDate = $Recycler.GetDetailsOf($item, 2) -replace "\u200f|\u200e",""
		$deletedDateTime = Get-Date $deletedDate
		$timeSpanSinceDeletion = New-TimeSpan -Start $deletedDateTime -End $(Get-Date)

		if ($timeSpanSinceDeletion.Days -gt $DaysTilExpiration) {
			if ($PSBoundParameters["Verbose"]) {
				Write-Output $item.Name
			}

			Remove-Item -Path $item.Path -Confirm:$false -Force -Recurse
		}
	}
}
