﻿using Newtonsoft.Json;
using System.IO;

namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// Static methods for json files.
    /// </summary>
    public static class JsonFileFormatter
    {
        /// <summary>
        /// Reads a json file and returns an object cooresponding to contents of the json file.
        /// </summary>
        /// <typeparam name="T">Object type cooresponding to the json file.</typeparam>
        /// <param name="filename">String of the file path. Can be absolute or relative, and does not need to exist.</param>
        /// <param name="jsonDataObj">Object cooresponding to the contents of the json file.</param>
        /// <returns>True if file exist; false otherwise.</returns>
        public static bool Read<T>(string filename, out T jsonDataObj)
        {
            JsonSerializer jsonSer = new JsonSerializer();

            using (StreamReader sr = new StreamReader(File.Open(filename, FileMode.OpenOrCreate)))
            {
                using (JsonReader jsonReader = new JsonTextReader(sr))
                {
                    jsonDataObj = jsonSer.Deserialize<T>(jsonReader);
                }
            }

            return (jsonDataObj != null);
        }

        /// <summary>
        /// Writes the json data object to a json file.
        /// </summary>
        /// <typeparam name="T">Type of the json data object.</typeparam>
        /// <param name="filename">String of the file path. Can be absolute or relative, and does not need to exist.</param>
        /// <param name="jsonDataObj">Object cooresponding to the contents of the json file.</param>
        /// <returns>True if the write was successful; false otherwise.</returns>
        public static bool Write<T>(string filename, T jsonDataObj)
        {
            JsonSerializer jsonSer = new JsonSerializer();

            using (StreamWriter sw = new StreamWriter(File.Open(filename, FileMode.Create)))
            {
                using (JsonWriter jsonWriter = new JsonTextWriter(sw))
                {
                    jsonSer.Serialize(jsonWriter, jsonDataObj, typeof(T));
                }
            }

            return true;
        }
    }
}
