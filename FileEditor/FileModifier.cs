﻿namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// Mimics the FileInfo class for file systems that are not part of System.IO. 
    /// </summary>
    /// <see cref="https://msdn.microsoft.com/en-us/library/system.io.fileinfo(v=vs.110).aspx"/>
    public abstract class FileModifier : FileSystemModifer
    {
    }
}
