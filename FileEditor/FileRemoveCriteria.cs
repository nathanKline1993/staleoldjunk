﻿using System.IO;

namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// Contains methods for determining whether a directory or file should be deleted or not.
    /// </summary>
    public abstract class FileRemoveCriteria
    {
        /// <summary>
        /// Checks whether a directory is empty or not. Intended to be overwritten for additional conditions.
        /// </summary>
        /// <param name="directoryInfo">DirectoryInfo of the directory being tested.</param>
        /// <returns>True if the directory is empty, false otherwise.</returns>
        public virtual bool CanRemoveDirectory(DirectoryInfo directoryInfo)
        {
            return DirectoryUtilities.IsDirectoryEmpty(directoryInfo);
        }

        /// <summary>
        /// Overridden to determine if a file can be removed or not.
        /// </summary>
        /// <param name="fileInfo">FileInfo of the file being tested.</param>
        /// <returns>True if the file can be deleted, false otherwise.</returns>
        public abstract bool CanRemoveFile(FileInfo fileInfo);
    }
}
