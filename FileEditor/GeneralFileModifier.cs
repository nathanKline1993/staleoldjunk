﻿using System;
using System.IO;

namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// Inherits FileModifier. FileModifier for System.IO.
    /// </summary>
    public class GeneralFileModifier : FileModifier
    {
        private FileInfo fileInfo;

        /// <summary>
        /// FileInfo FullName.
        /// </summary>
        public override string FullName => fileInfo.FullName;

        /// <summary>
        /// FileInfo LastAccessTime.
        /// </summary>
        public override DateTime LastAccessTime { get => fileInfo.LastAccessTime; set => fileInfo.LastAccessTime = value; }

        /// <summary>
        /// FileInfo Name.
        /// </summary>
        public override string Name => fileInfo.Name;

        /// <summary>
        /// Creates FileInfo object using path.
        /// </summary>
        /// <param name="path">String of the file's path.</param>
        public GeneralFileModifier(string path)
        {
            fileInfo = new FileInfo(path);
        }

        /// <summary>
        /// Deletes the file.
        /// </summary>
        public override void Delete()
        {
            fileInfo.Delete();
        }
    }
}
