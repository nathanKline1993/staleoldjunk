﻿using System;
using System.IO;

namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// Static methods specifically for directories. Using System.IO.
    /// </summary>
    public static class DirectoryUtilities
    {
        /// <summary>
        /// Uses the array returned by Directory.GetDirectories().
        /// </summary>
        /// <param name="directory">The directory of interest.</param>
        /// <returns>The number of directories in the directory of interest.</returns>
        public static int DirectoryCountInDirectory(DirectoryInfo directory)
        {
            String[] directories = Directory.GetDirectories(directory.FullName);

            return directories.Length;
        }

        /// <summary>
        /// Uses the array returned by Directory.GetFiles().
        /// </summary>
        /// <param name="directory">The directory of interest.</param>
        /// <returns>The number of files in the directory of interest.</returns>
        public static int FileCountInDirectory(DirectoryInfo directory)
        {
            String[] files = Directory.GetFiles(directory.FullName);

            return files.Length;
        }

        /// <summary>
        /// Looks at the number of files and directories in a given directory and determines if it is empty.
        /// </summary>
        /// <param name="directory">DirectoryInfo for the given directory.</param>
        /// <returns>True if the directory has 0 files and 0 directories, false otherwise.</returns>
        public static bool IsDirectoryEmpty(DirectoryInfo directory)
        {
             return DirectoryCountInDirectory(directory) == 0 && FileCountInDirectory(directory) == 0;
        }
    }
}
