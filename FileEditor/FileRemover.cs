﻿using System.Collections.Generic;
using System.IO;

namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// Static methods for deleting files in a specific manner.
    /// </summary>
    public static class FileRemover
    {
        /// <summary>
        /// Adds name of file or directory that was removed to list if it is not null.
        /// </summary>
        /// <param name="listOfFilesRemoved">List to add the file and directory names to.</param>
        /// <param name="nameOfFileRemoved">Name of file or directory.</param>
        private static void addToListOfFilesRemoved(List<string> listOfFilesRemoved, string nameOfFileRemoved)
        {
            if (listOfFilesRemoved != null)
            {
                listOfFilesRemoved.Add(nameOfFileRemoved);
            }
        }

        /// <summary>
        /// Deletes all the directories and files based on criteria of a given directory.
        /// </summary>
        /// <param name="fileDirectory">DirectoryInfo of the directory.</param>
        /// <param name="criteria">FileRemoveCriteria determine whether or directories and files can be deleted.</param>
        /// <param name="removeRoot">Delete fileDirectory if true, don't delete fileDirectory if false.</param>
        /// <param name="listOfFilesRemoved">If not null, will record the names of files and directories deleted.</param>
        /// <returns>True if all the files and directories were deleted based off criteria, false otherwise.</returns>
        public static bool RemoveFileBasedOnCriteria(DirectoryInfo fileDirectory, FileRemoveCriteria criteria, bool removeRoot, 
            List<string> listOfFilesRemoved = null)
        {
            bool deletedAllFilesBasedOnCriteria = true;

            foreach (DirectoryInfo directory in fileDirectory.EnumerateDirectories())
            {
                if (!RemoveFileBasedOnCriteria(directory, criteria, true, listOfFilesRemoved))
                {
                    deletedAllFilesBasedOnCriteria = false;
                }
            }

            foreach (FileInfo file in fileDirectory.EnumerateFiles())
            {
                if (criteria.CanRemoveFile(file))
                {
                    addToListOfFilesRemoved(listOfFilesRemoved, file.Name);
                    file.Delete();
                }
            }

            if (removeRoot && criteria.CanRemoveDirectory(fileDirectory))
            {
                addToListOfFilesRemoved(listOfFilesRemoved, fileDirectory.Name);
                fileDirectory.Delete();
            }

            return deletedAllFilesBasedOnCriteria;
        }
    }
}
