﻿using System;
using System.IO;

namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// Static methods for files and directories.
    /// </summary>
    public static class FileUtilities
    {
        /// <summary>
        /// Compares the DateTime.Now with a file's last access time to determine if the file is old.
        /// </summary>
        /// <param name="file">FileSystemInfo of the file.</param>
        /// <param name="timeUntilOld">TimeSpan determining what is considered old.</param>
        /// <returns>True if the file is old, false otherwise.</returns>
        public static bool IsFileOld(FileSystemInfo file, TimeSpan timeUntilOld)
        {
            DateTime now = DateTime.Now;
            TimeSpan fileLifeSpan = now - file.LastAccessTime;

            return fileLifeSpan > timeUntilOld;
        }
    }
}
