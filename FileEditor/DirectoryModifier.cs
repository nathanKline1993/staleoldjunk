﻿using System.Collections.Generic;

namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// Mimics the DirectoryInfo class for file systems that are not part of System.IO.
    /// </summary>
    /// <see cref="https://msdn.microsoft.com/en-us/library/system.io.directoryinfo(v=vs.110).aspx"/>
    public abstract class DirectoryModifier : FileSystemModifer
    {
        /// <summary>
        /// Overridden to delete files recursively.
        /// </summary>
        /// <param name="recursive">Whether or not the given directory should be deleted recursively</param>
        public abstract void Delete(bool recursive);

        /// <summary>
        /// Overridden to return a List or another IEnumerable.
        /// </summary>
        /// <returns>IEnumerable of all the directories in a given directory.</returns>
        public abstract IEnumerable<DirectoryModifier> EnumerateDirectories();

        /// <summary>
        /// Overridden to return a List or another IEnumerable.
        /// </summary>
        /// <returns>IEnumerable of all the files in a given directory.</returns>
        public abstract IEnumerable<FileModifier> EnumerateFiles();

        /// <summary>
        /// Overridden to return an array.
        /// </summary>
        /// <returns>Array of all the directories in a given directory.</returns>
        public abstract DirectoryModifier[] GetDirectories();

        /// <summary>
        /// Overridden to return an array.
        /// </summary>
        /// <returns>Array of all the files in a given directory.</returns>
        public abstract FileModifier[] GetFiles();

        /// <summary>
        /// Overridden to determine if a directory is empty or not.
        /// </summary>
        /// <returns>True if directory is empty, false otherwise.</returns>
        public abstract bool IsEmpty();
    }
}
