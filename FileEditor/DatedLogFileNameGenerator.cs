﻿using System;
using System.IO;

namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// FileNameGenerator class specifically for dated log files.
    /// </summary>
    public class DatedLogFileNameGenerator : FileNameGenerator
    {
        /// <summary>
        /// Main constructor, sets the TemplateName property.
        /// </summary>
        /// <param name="templateName">String set to TemplateName. Will be included in the filename.</param>
        public DatedLogFileNameGenerator(string templateName)
        {
            TemplateName = templateName;
        }

        /// <summary>
        /// Generates the filename based off the files contained in the specific directory. If a file exist with the initial proposed name
        /// the filename will incremented.
        /// </summary>
        /// <param name="directory">Directory that is search through. Only looks at the top directory; no children directories.</param>
        /// <returns>The appropriate filename for a file to be created in the specific directory.</returns>
        public override string GenerateFileName(DirectoryInfo directory)
        {
            DateTime today = DateTime.Today;
            string todayDateInFileNameStr = String.Format("{0}_{1}_{2}", today.Month, today.Day, today.Year);
            string filenameBasis = TemplateName + "_" + todayDateInFileNameStr;

            FileInfo[] duplicateFiles = directory.GetFiles("*" + filenameBasis + "*", SearchOption.TopDirectoryOnly);
            int numAppendToFileName = duplicateFiles.Length;

            if (numAppendToFileName == 0)
            {
                return filenameBasis + ".log";
            }
            
            while (File.Exists(directory.FullName + "\\" + filenameBasis + "_" + numAppendToFileName + ".log"))
            {
                numAppendToFileName++;
            }

            return filenameBasis + "_" + numAppendToFileName + ".log";
        }
    }
}
