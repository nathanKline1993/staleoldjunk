﻿using System;
using System.IO;

namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// Static class for filing data in a file once.
    /// </summary>
    public static class FileRecordFiler
    {
        /// <summary>
        /// Files data in a new file that will placed in the given directory.
        /// </summary>
        /// <param name="text">The data as a string.</param>
        /// <param name="directory">The directory of interest.</param>
        /// <param name="filename">Name of the file that will be placed in the directory.</param>
        /// <returns>Returns true or false if the operation was successful. Defaults to true right now; may change in the future.</returns>
        public static bool FileTextInDirectory(string text, DirectoryInfo directory, string filename)
        {
            File.WriteAllText(directory.FullName + "\\" + filename, text);

            return true;
        }

        /// <summary>
        /// Uses a FileNameGenerator instead of filename string.
        /// </summary>
        /// <param name="text">The data as a string.</param>
        /// <param name="directory">The directory of interest.</param>
        /// <param name="fileNameGenerator">FileNameGenerator that will create the filename.</param>
        /// <returns>Returns true or false if the operation was successful. Defaults to true right now; may change in the future.</returns>
        public static bool FileTextInDirectory(string text, DirectoryInfo directory, FileNameGenerator fileNameGenerator)
        {
            return FileTextInDirectory(text, directory, fileNameGenerator.GenerateFileName(directory));
        }
    }
}
