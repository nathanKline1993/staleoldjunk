﻿using System.IO;

namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// Abstract class for generating filenames. When you need to create a file, but you don't entirely care what the file's name is.
    /// </summary>
    public abstract class FileNameGenerator
    {
        /// <summary>
        /// String that should be included in the filename.
        /// </summary>
        public string TemplateName { get; set; }

        /// <summary>
        /// GenerateFileName method when you want to place a file in a specific directory.
        /// </summary>
        /// <param name="directory">The directory of interest.</param>
        /// <returns>Filename appropriate for directory.</returns>
        public abstract string GenerateFileName(DirectoryInfo directory);
    }
}
