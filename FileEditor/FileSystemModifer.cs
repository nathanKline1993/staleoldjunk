﻿using System;

namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// Mimics the FileSystemInfo class for file systems that are not part of System.IO.
    /// </summary>
    /// <see cref="https://msdn.microsoft.com/en-us/library/system.io.filesysteminfo(v=vs.110).aspx"/>
    public abstract class FileSystemModifer
    {
        /// <summary>
        /// Full name of the file or directory.
        /// </summary>
        /// <see cref="https://msdn.microsoft.com/en-us/library/system.io.filesysteminfo.fullname(v=vs.110).aspx"/>
        public abstract string FullName { get; }

        /// <summary>
        /// Last access time of the file or directory.
        /// </summary>
        /// <see cref="https://msdn.microsoft.com/en-us/library/system.io.filesysteminfo.lastaccesstime(v=vs.110).aspx"/>
        public abstract DateTime LastAccessTime { get; set; }

        /// <summary>
        /// Short name of the file or directory.
        /// </summary>
        /// <see cref="https://msdn.microsoft.com/en-us/library/system.io.filesysteminfo.name(v=vs.110).aspx"/>
        public abstract string Name { get; }

        /// <summary>
        /// Overridden to delete files or directories.
        /// </summary>
        public abstract void Delete();
    }
}
