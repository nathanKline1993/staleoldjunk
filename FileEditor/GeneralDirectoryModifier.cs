﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Com.Me.Nwk25.FileEditor
{
    /// <summary>
    /// Inherits DirectoryModifier. DirectoryModifier for System.IO.
    /// </summary>
    public class GeneralDirectoryModifier : DirectoryModifier
    {
        private DirectoryInfo directoryInfo;

        /// <summary>
        /// DirectoryInfo FullName.
        /// </summary>
        public override string FullName => directoryInfo.FullName;

        /// <summary>
        /// DirectoryInfo LastAccessTime.
        /// </summary>
        public override DateTime LastAccessTime { get => directoryInfo.LastAccessTime; set => directoryInfo.LastAccessTime = value; }

        /// <summary>
        /// DirectoryInfo Name.
        /// </summary>
        public override string Name => directoryInfo.Name;

        /// <summary>
        /// Creates DirectoryInfo object using path.
        /// </summary>
        /// <param name="path">String of the directory's path.</param>
        public GeneralDirectoryModifier(string path)
        {
            directoryInfo = new DirectoryInfo(path);
        }

        /// <summary>
        /// Deletes directory if not empty.
        /// </summary>
        public override void Delete()
        {
            if (IsEmpty())
            {
                directoryInfo.Delete();
            }
        }

        /// <summary>
        /// Deletes directory.
        /// </summary>
        /// <param name="recursive">Deletes the directory's content if true; runs Delete() if false.</param>
        public override void Delete(bool recursive)
        {
            if (recursive)
            {
                directoryInfo.Delete(true);
            } else
            {
                Delete();
            }
        }

        /// <summary>
        /// Collects all of the directories in the directory using the DirectoryInfo.
        /// </summary>
        /// <returns>IEnumerable of all the directories in the directory.</returns>
        public override IEnumerable<DirectoryModifier> EnumerateDirectories()
        {
            List<DirectoryModifier> directoryModifiers = new List<DirectoryModifier>();

            foreach (DirectoryInfo directory in directoryInfo.EnumerateDirectories())
            {
                GeneralDirectoryModifier generalDirectoryModifier = new GeneralDirectoryModifier(directory.FullName);
                directoryModifiers.Add(generalDirectoryModifier);
            }

            return directoryModifiers;
        }

        /// <summary>
        /// Collects all of the files in the directory using the DirectoryInfo.
        /// </summary>
        /// <returns>IEnumerable of all the files in the directory.</returns>
        public override IEnumerable<FileModifier> EnumerateFiles()
        {
            List<FileModifier> fileModifiers = new List<FileModifier>();

            foreach (FileInfo file in directoryInfo.EnumerateFiles())
            {
                GeneralFileModifier generalFileModifier = new GeneralFileModifier(file.FullName);
                fileModifiers.Add(generalFileModifier);
            }

            return fileModifiers;
        }

        /// <summary>
        /// Collects all of the directories in an array.
        /// </summary>
        /// <returns>Array of all directories in the directory.</returns>
        public override DirectoryModifier[] GetDirectories()
        {
            DirectoryInfo[] dirInfoArray = directoryInfo.GetDirectories();
            List<GeneralDirectoryModifier> dirModList = new List<GeneralDirectoryModifier>();

            foreach (DirectoryInfo directory in dirInfoArray)
            {
                GeneralDirectoryModifier dirModifier = new GeneralDirectoryModifier(directory.FullName);
                dirModList.Add(dirModifier);
            }

            return dirModList.ToArray();
        }

        /// <summary>
        /// Collects all of the files in an array.
        /// </summary>
        /// <returns>Array of all directories in the directory.</returns>
        public override FileModifier[] GetFiles()
        {
            FileInfo[] fileInfoArray = directoryInfo.GetFiles();
            List<GeneralFileModifier> fileModList = new List<GeneralFileModifier>();

            foreach (FileInfo file in fileInfoArray)
            {
                GeneralFileModifier fileModifier = new GeneralFileModifier(file.FullName);
                fileModList.Add(fileModifier);
            }

            return fileModList.ToArray();
        }

        /// <summary>
        /// Determines whether the directory is empty or not.
        /// </summary>
        /// <returns>True if empty, false otherwise.</returns>
        public override bool IsEmpty()
        {
            return DirectoryUtilities.IsDirectoryEmpty(directoryInfo);
        }
    }
}
