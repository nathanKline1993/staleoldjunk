﻿using Com.Me.Nwk25.UniversalWebAPIClient;
using Com.Me.Nwk25.FileEditor;
using System.IO;

namespace Com.Me.Nwk25.DesktopAlerts
{
    /// <summary>
    /// SMSMessageCarrier that uses email to send the SMS message.
    /// </summary>
    public class EmailToSMSMessageCarrier: SMSMessageCarrier
    {
        private EmailWebAPIClient emailWebAPIClient = null;
        private int messageSizeLimit;
        private string hostname;

        /// <summary>
        /// Location of the log file in case the SMS message will be large. May be null.
        /// </summary>
        public override DirectoryInfo LogFileLocation { get; set; }

        /// <summary>
        /// Responsible for generating the name of the log file. May be null.
        /// </summary>
        public override FileNameGenerator FileNameGen { get; set; }

        /// <summary>
        /// Main constructor that uses a EmailWebAPIClient to send the email.
        /// </summary>
        /// <param name="cellCarrier">Cell carrier of the phone you want to send the SMS message to.</param>
        /// <param name="recipient">The phone number.</param>
        /// <param name="emailWebAPIClient">Used to send the SMS message.</param>
        /// <param name="logFileLocation">Location of the log files in case the message is too large to send.</param>
        /// <param name="fileNameGenerator">Generates names for the log files that will be placed in logFileLocation.</param>
        public EmailToSMSMessageCarrier(CellCarrier cellCarrier, string recipient, EmailWebAPIClient emailWebAPIClient, 
            DirectoryInfo logFileLocation = null, FileNameGenerator fileNameGenerator = null)
        {
            setCellCarrierInfoVariables(cellCarrier);
            Recipient = recipient;
            this.emailWebAPIClient = emailWebAPIClient;
            LogFileLocation = logFileLocation;
            FileNameGen = fileNameGenerator;
        }

        /// <summary>
        /// Ensures that I'm sending the email to the correct SMS email server and the message size is not too large.
        /// </summary>
        /// <param name="cellCarrier">Cell carrier of the phone you want to send the SMS message to.</param>
        private void setCellCarrierInfoVariables(CellCarrier cellCarrier)
        {
            switch (cellCarrier)
            {
                case CellCarrier.ATAndT:
                    hostname = Properties.Resources.ATAndTSMSHostName;
                    messageSizeLimit = 160;
                    break;
                case CellCarrier.Verizon:
                    hostname = Properties.Resources.VerizonSMSHostName;
                    messageSizeLimit = 140;
                    break;
            }
        }

        /// <summary>
        /// If the SMS message is not sent the message will be saved in a log file. This only works if a
        /// logFileLocation and fileNameGenerator were passed through the constructor.
        /// </summary>
        /// <param name="message">The SMS message to be sent.</param>
        /// <returns>True if the message was sent. False otherwise.</returns>
        public override bool SendSMSMessage(string message)
        {
            bool messageSizeIsAcceptable = false;
            bool messageWasSent = false;
            string recipientEmailAddress = Recipient + "@" + hostname;

            messageSizeIsAcceptable = (message.Length + recipientEmailAddress.Length) <= messageSizeLimit;
            messageWasSent = messageSizeIsAcceptable && emailWebAPIClient.SendMessage(recipientEmailAddress, "", message);

            if (!messageWasSent && LogFileLocation != null && FileNameGen != null)
            {
                string filename = FileNameGen.GenerateFileName(LogFileLocation);

                File.WriteAllText(LogFileLocation.FullName + "\\" + filename, message);
                messageWasSent = emailWebAPIClient.SendMessage(recipientEmailAddress, "", 
                    string.Format(Properties.Resources.DefaultMessageWhenTooLarge, filename));
            }

            return messageWasSent;
        }
    }
}
