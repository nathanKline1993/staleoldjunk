﻿using System;
using System.Collections.Generic;

namespace Com.Me.Nwk25.DesktopAlerts
{
    /// <summary>
    /// Cell carriers that DesktopAlerts supports. Currently DesktopAlerts supports only AT&T and Verizon.
    /// </summary>
    public enum CellCarrier
    {
        ATAndT,
        Verizon
    }

    /// <summary>
    /// Static class for translating the CellCarrier enum when used by a GUI. Solely created for the purpose of making CellCarrier.ATAndT 
    /// more readable.
    /// </summary>
    public static class CellCarrierEnumTranslator
    {
        /// <summary>
        /// Gets the name of the CellCarrier enum value.
        /// </summary>
        /// <param name="cellCarrier">CellCarrier enum value.</param>
        /// <returns>Returns the string of the name of the CellCarrier enum value, unless it is CellCarrier.ATAndT. Then it will 
        /// return "AT&T"</returns>
        public static string GetName(CellCarrier cellCarrier)
        {
            string name = Enum.GetName(typeof(CellCarrier), cellCarrier);

            if (name.Equals("ATAndT"))
            {
                return "AT&T";
            }

            return name;
        }

        /// <summary>
        /// Uses GetName to collect all of the names of the CellCarrier enum values.
        /// </summary>
        /// <returns>An array of strings. Each string is a name of a CellCarrier enum value.</returns>
        public static string[] GetNames()
        {
            List<string> listOfCellCarrierNames = new List<string>();

            foreach (object value in Enum.GetValues(typeof(CellCarrier)))
            {
                CellCarrier cellCarrierValue = (CellCarrier)value;

                listOfCellCarrierNames.Add(GetName(cellCarrierValue));
            }

            return listOfCellCarrierNames.ToArray();
        }

        /// <summary>
        /// Attempts to parse a string for only a CellCarrier enum.
        /// </summary>
        /// <param name="cellCarrierStr">String to parsed.</param>
        /// <param name="cellCarrier">If successful, populated with the CellCarrier enum value specified in cellCarrierStr.</param>
        /// <returns>True if the parse was successful, false otherwise.</returns>
        public static bool TryParse(string cellCarrierStr, out CellCarrier cellCarrier)
        {
            if (cellCarrierStr.Equals("AT&T"))
            {
                cellCarrierStr = "ATAndT";
            }

            return Enum.TryParse<CellCarrier>(cellCarrierStr, out cellCarrier);
        }
    }
}
