﻿using Com.Me.Nwk25.FileEditor;
using System.IO;
using System.Text.RegularExpressions;

namespace Com.Me.Nwk25.DesktopAlerts
{
    /// <summary>
    /// Abstract for class for SMS messaging.
    /// </summary>
    public abstract class SMSMessageCarrier
    {
        private string recipient;

        /// <summary>
        /// Log file location in case the SMS message is too large.
        /// </summary>
        public abstract DirectoryInfo LogFileLocation { get; set; }

        /// <summary>
        /// Generates the name of the log file.
        /// </summary>
        public abstract FileNameGenerator FileNameGen { get; set; }

        /// <summary>
        /// Recipient of the SMS message.
        /// </summary>
        public string Recipient {
            get
            {
                return recipient;
            }
            set
            {
                recipient = getOnlyNumsFromPhoneNumStr(value);
            }
        }

        /// <summary>
        /// Gets only the numbers of a phone number string.
        /// </summary>
        /// <param name="phoneNumStr">Phone number string</param>
        /// <returns>String of the phone number with only the numbers; no '(', ')', '-', or ' '.</returns>
        private string getOnlyNumsFromPhoneNumStr(string phoneNumStr)
        {
            const int numOfNumsInPhoneNum = 10;
            Regex validCharactersInPhoneNumStr = new Regex(@"\d+");
            string phoneNumStrWithOnlyNums = "";
            
            MatchCollection matchCollection = validCharactersInPhoneNumStr.Matches(phoneNumStr);

            foreach (Match match in matchCollection)
            {
                if (match.Success)
                {
                    phoneNumStrWithOnlyNums += match.Value;
                }
            }

            if (phoneNumStrWithOnlyNums.Length != numOfNumsInPhoneNum)
            {
                throw new InvalidRecipientException("Invalid phone number");
            }

            return phoneNumStrWithOnlyNums;
        }

        /// <summary>
        /// Abstract method for sending the SMS message to the Recipient.
        /// </summary>
        /// <param name="message">Message to be sent.</param>
        /// <returns>True if the SMS message was successfully sent, false otherwise.</returns>
        public abstract bool SendSMSMessage(string message);
    }
}
