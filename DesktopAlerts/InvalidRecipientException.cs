﻿using System;

namespace Com.Me.Nwk25.DesktopAlerts
{
    /// <summary>
    /// Exception for invalid recipient formatting or other reasons.
    /// </summary>
    public class InvalidRecipientException : Exception
    {
        /// <summary>
        /// Calls Exception(string) constructor.
        /// </summary>
        /// <param name="message">Message assoicated with exception. Passed to the Exception constructor.</param>
        public InvalidRecipientException(string message) : base(message) { }
    }
}
