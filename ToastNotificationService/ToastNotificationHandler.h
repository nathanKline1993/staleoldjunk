#pragma once
#pragma comment(lib, "runtimeobject")

#include <comdef.h>
#include <wrl.h>
#include <Windows.h>
#include <winreg.h>
#include <WinUser.h>

#include "ToastNotificationService.h"

using namespace Microsoft::WRL;

/* Inspired by: 
 * https://blogs.msdn.microsoft.com/tiles_and_toasts/2015/10/16/quickstart-handling-toast-activations-from-win32-apps-in-windows-10/
 */
typedef struct _NOTIFICATION_USER_INPUT_DATA {
	LPCWSTR Key;
	LPCWSTR Value;
} NOTIFICATION_USER_INPUT_DATA ;

/* Inspired by: 
 * https://blogs.msdn.microsoft.com/tiles_and_toasts/2015/10/16/quickstart-handling-toast-activations-from-win32-apps-in-windows-10/
 */
__interface DECLSPEC_UUID("53E31837-6600-4A81-9395-75CFFE746F94") INotificationActivationCallback : IUnknown
{
	HRESULT Activate(
		LPCWSTR appUserModelId,
		LPCWSTR arguments,
		const NOTIFICATION_USER_INPUT_DATA* data,
		ULONG count
	);
};

/* Inspired by: 
 * https://blogs.msdn.microsoft.com/tiles_and_toasts/2015/10/16/quickstart-handling-toast-activations-from-win32-apps-in-windows-10/
 */
class DECLSPEC_UUID("23A5B06E-20BB-4E7E-A0AC-6982ED6A6041") NotificationActivator
	WrlSealed : public RuntimeClass<RuntimeClassFlags<ClassicCom>, INotificationActivationCallback> WrlFinal
{
public:
	virtual HRESULT Activate(
		LPCWSTR appUserModelId,
		LPCWSTR arguments,
		const NOTIFICATION_USER_INPUT_DATA* data,
		ULONG count
	) override {
		/* TODO: Make more abstract. */
		//if (wcscmp(appUserModelId, L"Com.Me.Nwk25.StaleOldJunkGUI") == 0) {
		//}
		MessageBox(nullptr, L"Hey you guys!!!! The toast worked.", L"Toast Test",
			MB_ICONINFORMATION | MB_TASKMODAL);
	};
};

/* Inspired by: 
 * https://github.com/WindowsNotifications/desktop-toasts/blob/master/CPP/DesktopToastsSample.cpp
 */
class ToastNotificationHandler
{
private:
	static ToastNotificationHandler *singleInstance;

	HRESULT RegisterActivator();
	HRESULT RegisterAppForNotificationSupport();
	void UnregisterActivator();
public:
	ToastNotificationHandler();
	~ToastNotificationHandler();

	HRESULT Initialize();
	static ToastNotificationHandler *GetInstance();
};

