/* Inspired by: 
 * https://github.com/WindowsNotifications/desktop-toasts/blob/master/CPP/DesktopToastsSample.cpp
 */

#include "ToastNotificationHandler.h"

ToastNotificationHandler *ToastNotificationHandler::singleInstance = nullptr;

ToastNotificationHandler::ToastNotificationHandler()
{
	singleInstance = this;
}

ToastNotificationHandler::~ToastNotificationHandler()
{
	UnregisterActivator();
	singleInstance = nullptr;
}

HRESULT ToastNotificationHandler::RegisterActivator()
{
	/* Module<OutOfProc> needs a callback; I'm passing an empty callback because I don't care. */
	Module<OutOfProc>::Create([] {});

	Module<OutOfProc>::GetModule().IncrementObjectCount();

	return Module<OutOfProc>::GetModule().RegisterObjects();
}

HRESULT ToastNotificationHandler::RegisterAppForNotificationSupport()
{
	wofstream testLogFile;

	PCWSTR exePath = L"\"C:\\Users\\dolfa\\OneDrive\\Documents\\Visual Studio 2017\\Projects\\StaleOldJunk\\Debug\\ToastNotificationService.exe\"";

	DWORD dataSize = static_cast<DWORD>((::wcslen(exePath) + 1) * sizeof(WCHAR));

	PHKEY toastNotificationRegKeyPtr = nullptr;

	//HRESULT hr = HRESULT_FROM_WIN32(RegCreateKeyEx(
	//	HKEY_CURRENT_USER,
	//	LR"(Software\Classes\CLSID\{23A5B06E-20BB-4E7E-A0AC-6982ED6A6041}\LocalServer32)",
	//	0, 
	//	nullptr,
	//	REG_OPTION_VOLATILE,
	//	KEY_ALL_ACCESS,
	//	nullptr,
	//	toastNotificationRegKeyPtr,
	//	nullptr));

	//_com_error err(hr);

	//hr |= HRESULT_FROM_WIN32(RegSetValueEx(
	//	*toastNotificationRegKeyPtr,
	//	nullptr,
	//	0,
	//	REG_SZ,
	//	reinterpret_cast<const BYTE *>(exePath),
	//	dataSize));

	//hr |= HRESULT_FROM_WIN32(RegCloseKey(*toastNotificationRegKeyPtr));

	return S_OK;
}

void ToastNotificationHandler::UnregisterActivator()
{
	Module<OutOfProc>::GetModule().UnregisterObjects();
	Module<OutOfProc>::GetModule().DecrementObjectCount();
}

HRESULT ToastNotificationHandler::Initialize()
{
	HRESULT hr = RegisterAppForNotificationSupport();


	if (SUCCEEDED(hr)) {
		hr = RegisterActivator();
	}

	return hr;
}

ToastNotificationHandler *ToastNotificationHandler::GetInstance()
{
	return singleInstance;
}