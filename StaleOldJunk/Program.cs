﻿using Com.Me.Nwk25.StaleOldJunkLogic;
using Com.Me.Nwk25.UWPToWinAppUtility;
using System;
using System.Diagnostics;

namespace Com.Me.Nwk25.StaleOldJunk
{
    /// <summary>
    /// Main class for StaleOldJunk.
    /// </summary>
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">Not used currently.</param>
        static void Main(string[] args)
        {
            UWPToWinAppDecorator staleOldJunkDecoratorComponents = StaleOldJunkDecoratorFactory.CreateStaleOldJunkComponent();

            if (!staleOldJunkDecoratorComponents.CallTrailer())
            {
                Process.Start(StaleOldJunkLogicResourcesTranslator.StaleOldJunkGUIExeName);
                Environment.Exit(0);
            }
        }
    }
}
