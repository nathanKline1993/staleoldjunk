﻿namespace Com.Me.Nwk25.UniversalWebAPIClient
{
    /// <summary>
    /// Abstract class for using a HTTP email API.
    /// </summary>
    public abstract class EmailWebAPIClient
    {
        /// <summary>
        /// Name of the application using the email API.
        /// </summary>
        public abstract string ApplicationName { get; }

        /// <summary>
        /// Email address of the user using the client.
        /// </summary>
        public abstract string EmailAddress { get; }

        /// <summary>
        /// Sends an email where the sender is the user of this client. This one supports only text emails.
        /// </summary>
        /// <param name="to">Recipient's email address.</param>
        /// <param name="subject">Subject of the email message.</param>
        /// <param name="message">Body of the email.</param>
        /// <returns>True if the email was successful sent, otherwise false.</returns>
        public abstract bool SendMessage(string to, string subject, string message);
    }
}
