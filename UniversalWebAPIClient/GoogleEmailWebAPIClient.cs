﻿using AE.Net.Mail;
using Google;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Microsoft.IdentityModel.Tokens;
using System.IO;
using System.Threading;

namespace Com.Me.Nwk25.UniversalWebAPIClient
{
    /// <summary>
    /// EmailWebAPIClient specifically for Gmail.
    /// </summary>
    public class GoogleEmailWebAPIClient : EmailWebAPIClient
    {
        private GmailService gmailService;
        private string applicationName;
        private static string[] scopes = { GmailService.Scope.GmailReadonly, GmailService.Scope.GmailSend };
        private UserCredential userCredential;

        /// <summary>
        /// Returns the application name set in the constructor.
        /// </summary>
        public override string ApplicationName => applicationName;

        /// <summary>
        /// Returns the email address of the user that allowed permission for the client.
        /// </summary>
        public override string EmailAddress {
            get
            {
                Profile profile = gmailService.Users.GetProfile("me").Execute();
                return profile.EmailAddress;
            }
        }

        /// <summary>
        /// GoogleEmailWebAPIClient's constructor. Redirects you to Google login to grant the application access to your Google account or 
        /// retrieves an access file via a cached file. Also initializes the application name.
        /// </summary>
        /// <param name="applicationName">The name of the application associated with the client secret.</param>
        /// <param name="clientSecretJsonFilePath">Path to the json file that contains the application's client secret.</param>
        /// <param name="userCredentialFullFilePath">Path to the folder that will contain the cached access and refresh token.</param>
        public GoogleEmailWebAPIClient(string applicationName, string clientSecretJsonFilePath, string userCredentialFullFilePath)
        {
            using (FileStream clientSecretJsonFileStream = new FileStream(clientSecretJsonFilePath, FileMode.Open, FileAccess.Read))
            {
                userCredential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(clientSecretJsonFileStream).Secrets, 
                    scopes, 
                    "user", 
                    CancellationToken.None,
                    new FileDataStore(userCredentialFullFilePath, true)).Result;
            }

            gmailService = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = userCredential,
                ApplicationName = applicationName,
            });

            this.applicationName = applicationName;
        }


        /// <summary>
        /// Constructs the email message and sends it.
        /// </summary>
        /// <param name="to">The email address that the message will be sent to.</param>
        /// <param name="subject">Subject of the email message.</param>
        /// <param name="body">The content of the email.</param>
        /// <returns>True if an GoogleApiException is not thrown when attempting to send the message, otherwise false.</returns>
        public override bool SendMessage(string to, string subject, string body)
        {
            MailMessage mailMessage = new MailMessage()
            {
                From = new System.Net.Mail.MailAddress(EmailAddress),
                Subject = subject,
                Body = body,
            };

            mailMessage.To.Add(new System.Net.Mail.MailAddress(to));

            StringWriter stringWriter = new StringWriter();
            mailMessage.Save(stringWriter);

            string base64UrlEncodedString = Base64UrlEncoder.Encode(stringWriter.ToString());

            Message message = new Message();
            message.Raw = base64UrlEncodedString;

            try
            {
                Message result = gmailService.Users.Messages.Send(message, "me").Execute();
            } catch (GoogleApiException)
            {
                return false;
            }

            return true;
        }
    }
}
